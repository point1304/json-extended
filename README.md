### Description
extended version of default `json` module with `datetime`, `date` and `uuid` support

### Supported Platform
* __Python__ : cPython >= 3.6

### Install
#### via `pip` (recommended)
* __When python3 is the only python distribution installed on the system:__
```shell
pip install json_extended
```
* __With multiple versions of python distributions:__
```shell
python3 -m pip install json_extended
```